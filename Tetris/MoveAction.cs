﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tetris
{
    class MoveAction : UserAction
    {
        public GraphicCard[,] graphicCardsBoard { get; set; }
        public int xMove { get; set; }

        public MoveAction(GraphicCard[,] graphicCards, int xMove) : base()
        {
            this.xMove = xMove;
            this.graphicCardsBoard = graphicCards;
        }

        //move the shape
        public override void DoAction(Shape shape)
        {
            GraphicCard[,] tetromino = shape.Tetromino;
            if (this.xMove < 0)
            {
                if (this.CanMoveLeft(tetromino))
                {
                    this.MoveLeft(tetromino);
                    this.isValid = true;
                }
                else
                    isValid = false;
            }

            else if (this.xMove > 0)
            {
                if (this.CanMoveRight(tetromino))
                {
                    this.MoveRight(tetromino);
                    this.isValid = true; 
                }
                else
                    this.isValid = false;
            }
        }

        public void MoveLeft(GraphicCard[,] tetromino)
        {
            foreach (GraphicCard card in tetromino)
            {
                if(card != null)
                { 
                    lock(card)
                    {
                        card.posInBoard = (card.posInBoard.row, card.posInBoard.col - 1);
                        //this.graphicCardsBoard[card.posInBoard.row, card.posInBoard.col + 1] = null;
                        //this.graphicCardsBoard[card.posInBoard.row, card.posInBoard.col] = card;
                        card.X += xMove;
                    }              
                }
            }
        }

        //xMove - negative number
        private bool CanMoveLeft(GraphicCard[,] tetromino)
        {
            for (int i = 0; i < tetromino.GetLength(0); i++)
            {
                for (int j = 0; j < tetromino.GetLength(1); j++)
                {
                    GraphicCard card = tetromino[i, j];
                    if (card != null)
                    {
                        if (card.finalPosInBoard.col != 0 && graphicCardsBoard[card.posInBoard.row, card.posInBoard.col - 1] == null)
                            break;
                        else
                            return false;
                    }
                }
            }
            return true;
        }


        public void MoveRight(GraphicCard[,] tetromino)
        {
            foreach (GraphicCard card in tetromino)
            {
                if (card != null)
                {
                    card.posInBoard = (card.posInBoard.row, card.posInBoard.col + 1);
                    //this.graphicCardsBoard[card.posInBoard.row, card.posInBoard.col + 1] = null;
                    //this.graphicCardsBoard[card.posInBoard.row, card.posInBoard.col] = card;
                    card.X += xMove;
                }

            }
        }
        //xMove - positive number
        public bool CanMoveRight(GraphicCard[,] tetromino)
        {
            for (int i = 0; i < tetromino.GetLength(0); i++)
            {
                for (int j = tetromino.GetLength(0) - 1; j >= 0; j--)
                {
                    GraphicCard card = tetromino[i, j];
                    if (card != null)
                    {
                        if (card.finalPosInBoard.col != Constants.COLLUMS_NUMBER - 1
                            && graphicCardsBoard[card.posInBoard.row, card.posInBoard.col + 1] == null)
                            break;
                        else
                            return false;
                    }
                }
            }
            return true;
        }
    }
}