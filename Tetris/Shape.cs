﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using Android.Graphics;
using System.Linq;
using System.Text;
using System.Threading;

namespace Tetris
{
    public abstract class Shape
    {
        public GraphicCard[,] Tetromino { get; set; }
        public List<GraphicCard> activeCards { get; set; }
        public Context context { get; set; }
        public bool fullyOnBoard { get; set; }
        
        public Shape(int cardWidth, Context context)
        {
            this.Tetromino = new GraphicCard[4,4];
            this.activeCards = new List<GraphicCard>();
            Random rnd = new Random();
            int index = rnd.Next(0, Constants.BITMAPS_NAMES.Length);
            string color = Constants.BITMAPS_NAMES[index];
            int imageId = context.Resources.GetIdentifier(color, "drawable", context.PackageName);
            Bitmap bitmap = Android.Graphics.BitmapFactory.DecodeResource(Application.Context.Resources, imageId);
            for (int i = 0; i < Tetromino.GetLength(0); i++)
            {
                for (int j = 0; j < Tetromino.GetLength(0); j++)
                {
                     Tetromino[i, j] = new GraphicCard(bitmap, cardWidth, color); 
                }
            }
            this.context = context;
        }

        public Shape(GraphicCard card, Context context)
        {
            this.Tetromino = new GraphicCard[4, 4];
            this.activeCards = new List<GraphicCard>();
            this.Tetromino[2, 2] = new GraphicCard(card);
            this.activeCards.Add(this.Tetromino[2, 2]);
            this.context = context;
        }

        //check if the shape is fully inside the board 
        public void SetFullyOnBoard(GraphicBoard graphicBoard)
        {
            int boardTop = graphicBoard.Top;
            bool fullyOnBoard = true;
            foreach (GraphicCard card in this.activeCards)
            {
                if (card.Y >= boardTop)
                    fullyOnBoard = fullyOnBoard && true;
                else
                {
                    fullyOnBoard = false;
                    break;
                }
            }
            this.fullyOnBoard = fullyOnBoard;
            if (fullyOnBoard)
                this.SetCardsPosInBoard(graphicBoard);
        }

        //if the shape is fully inside the board, set its position
        public void SetCardsPosInBoard(GraphicBoard graphicBoard)
        {
            int tempTopRow = graphicBoard.GraphicCards.GetLength(0) - 1;
            for (int rowInTet = 0; rowInTet < this.Tetromino.GetLength(0); rowInTet++)
            {
                bool atLeastOneInRow = false;
                GraphicCard leftCard = null;
                for (int colInTet = 0; colInTet < this.Tetromino.GetLength(1); colInTet++)
                {
                    GraphicCard card = this.Tetromino[rowInTet, colInTet];
                    if (card != null)
                    {
                        card.onBoard = true;
                        atLeastOneInRow = true;
                        if (leftCard == null)
                            card.posInBoard = (tempTopRow, graphicBoard.GetColFromX(card.X));
                        else
                            card.posInBoard = (tempTopRow, leftCard.posInBoard.col + 1);
                        leftCard = card;
                    }
                    if (leftCard == null)
                        leftCard = card;
                }
                if (atLeastOneInRow)
                    tempTopRow--;
            }
        }


        //get the final point of falling of each card in the shape 
        public bool GetFinalPlaceInBoard(GraphicBoard graphicBoard)
        {
            for(int rowInTet = this.Tetromino.GetLength(0) - 1; rowInTet >= 0; rowInTet--)
            {
                List<GraphicPoint> trgtPoints = new List<GraphicPoint>();
                List<(int row, int col)> trgtIndexes = new List<(int, int)> ();
                int righestColInTet = Constants.NONE;
                for (int colInTet = 0; colInTet < this.Tetromino.GetLength(1); colInTet++)
                {
                    GraphicCard card = this.Tetromino[rowInTet, colInTet];
                    if (card != null)
                    {
                        righestColInTet = righestColInTet == -1 ? colInTet : righestColInTet;
                        int trgtCol = graphicBoard.GetColFromX(card.X);
                        int trgtRow = graphicBoard.GetFirstEmptyRowInCol(trgtCol, card.posInBoard.row);
                        if (trgtRow != Constants.NONE)
                        {
                            GraphicPoint trgtPoint = graphicBoard.GetPointPromPosInArray(trgtRow, trgtCol);
                            card.SetTargetPnt(trgtPoint);
                            trgtPoints.Add(trgtPoint);
                            trgtIndexes.Add((trgtRow, trgtCol));
                        }
                        else
                        {
                            card.SetTargetPnt(new GraphicPoint(card.X, card.Y));
                            return false;
                        }
                    }
                }
                if (trgtPoints.Count > 0)
                    this.SetHeightDiffInRow(rowInTet, righestColInTet, trgtIndexes, trgtPoints, graphicBoard);
            }

            this.SetNullInTrgt(graphicBoard);

            return true;
        }

        private void SetHeightDiffInRow(int rowInTet, int colInTet, List<(int row, int col)> trgtIndexes, 
            List<GraphicPoint> trgtPoints, GraphicBoard graphicBoard)
        {
            int highestY = trgtPoints.Min(p => p.Y);
            int highestRow = trgtIndexes.Max(pos => pos.row);

            foreach ((int row, int col) pos in trgtIndexes)
            {
                GraphicCard card = this.Tetromino[rowInTet, colInTet];
                card.SetTargetPnt(new GraphicPoint(card.targetPnt.X, highestY));
                graphicBoard.GraphicCards[highestRow, pos.col] = card; //set place in board obly for the lines above
                card.finalPosInBoard = ((highestRow, pos.col)); //set the final place for clean the board in the beginning of the falling
                colInTet++;
            }
        }

        public void SetNullInTrgt(GraphicBoard graphicBoard)
        {
            foreach(GraphicCard card in this.activeCards)
            {
                graphicBoard.GraphicCards[card.finalPosInBoard.row, card.finalPosInBoard.col] = null;
            }
        }
        

        //fall the shape
        public void Fall()
        {
            foreach (GraphicCard card in this.activeCards)
            {
                lock (card)
                {
                    card.Fall();
                }
            }
        }

        public void Draw(Canvas canvas)
        {
            lock(this.activeCards)
            {
                foreach (GraphicCard card in this.activeCards)
                {
                    card.Draw(canvas);
                }
            }
            
        }
        
        //set every card's target direction
        public void SetTargetDirection()
        {
            foreach (GraphicCard card in this.activeCards)
            {
                card.SetTargetDirection();
            }
        }

        //check if the shape has finished to fall 
        public bool OnTarget()
        {
            bool ans = false;
            foreach (GraphicCard card in this.activeCards)
            {
                ans = ans || card.OnTarget();
            }
            return ans;   
            //return this.activeCards[0].OnTarget();
        }

        public bool InSide(GraphicPoint p)
        {
            bool ans = false;
            foreach(GraphicCard card in activeCards)
            {
                ans = ans || card.InSide(p);
            }
            return ans;
        }

        //function that get a row and check if the shape or part of it is above this row
        public bool AboveRow(int row)
        {
            bool ans = false;

            foreach(GraphicCard card in this.activeCards)
            {
                ans = ans || card.posInBoard.row > row;
            }
            return ans;
        }

        //put null in places that has not have card
        protected void PutNullInEmptyPlaces()
        {
            for (int i = 0; i < Tetromino.GetLength(0); i++)
            {
                for (int j = 0; j < Tetromino.GetLength(0); j++)
                {
                    if (!Tetromino[i, j].HasPosSet())
                    {
                        Tetromino[i, j] = null;
                    }
                }
            }
        }

       
        public abstract void SetShape(GraphicBoard graphicBoard);

    }
}