﻿using System;
using System.Collections.Generic;
using System.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Linq;
using System.Text;
using SQLite;

namespace Tetris
{
    public class DBHandler
    {
        public string name { set; get; }
        public string path { set; get; }
        public SQLiteConnection db { set; get; }

        public DBHandler(string dbName)
        {
            this.name = dbName;
            this.path = System.IO.Path.Combine(
                System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), dbName);

            if (!File.Exists(this.path))
            {
                this.db = new SQLiteConnection(this.path);
                db.CreateTable<Result>();
                db.CreateTable<DBcard>();
            }
            else
            {
                this.db = new SQLiteConnection(this.path);
            }
        }


        public DBHandler(string dbName, List<string> tableNames)
        {
            this.name = dbName;
            this.path = System.IO.Path.Combine(
                System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), dbName);

            if (!File.Exists(this.path))
            {
                this.db = new SQLiteConnection(this.path);
                foreach(string name in tableNames)
                {
                    db.CreateTable(Type.GetType("Tetris." + name));
                }
            }
            else
            {
                this.db = new SQLiteConnection(this.path);
            }
        }

    }
}