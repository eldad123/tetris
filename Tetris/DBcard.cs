﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQLite;

namespace Tetris
{
    [Table("Cards")]

    public class DBcard
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { set; get; }
        public int X { get; set; }
        public int Y { get; set; }
        public int WIDTH { get; set; }
        public int ROW_IN_BOARD { get; set; }
        public int COL_IN_BOARD { get; set; }
        public string COLOR { get; set; }

        public DBcard(int x, int y, int width, int rowInBoard, int colInBoard, string color)
        {
            this.X = x;
            this.Y = y;
            this.WIDTH = width;
            this.ROW_IN_BOARD = rowInBoard;
            this.COL_IN_BOARD = colInBoard;
            this.COLOR = color;
        }
        public DBcard(GraphicCard card)
        {
            this.X = card.X;
            this.Y = card.Y;
            this.WIDTH = card.width;
            this.ROW_IN_BOARD = card.posInBoard.row;
            this.COL_IN_BOARD = card.posInBoard.col;
            this.COLOR = card.Color;
        }

        public DBcard()
        {

        }
    }
}