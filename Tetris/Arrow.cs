﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tetris
{
    public class Arrow
    {
        public Bitmap bitmap { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public double height { get; set; }
        public double width { get; set; }
        public string Side { get; set; }

        public Arrow(Bitmap bitmap, double x, double y, double height, double width, string side)
        {
            this.X = x;
            this.Y = y;
            this.bitmap = bitmap;
            this.height = height;
            this.width = width;
            this.Side = side;
        }

        public void Draw(Canvas canvas)
        {
            canvas.DrawBitmap(Bitmap.CreateScaledBitmap(this.bitmap, (int)width, (int)height, true),
                (float)X, (float)Y, null);
        }

        public bool InSide(GraphicPoint p)
        {
            return (p.X >= this.X && p.X <= this.X + this.width) && (p.Y >= this.Y && p.Y <= this.Y + this.height);
        }

    }
}