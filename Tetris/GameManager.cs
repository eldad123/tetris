﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using Tetris.Shapes;

namespace Tetris
{
    public class GameManager
    {
        public GraphicBoard graphicBoard { get; set; }
        public ShapesOnBoard ShapesOnBoard { get; set; }
        public bool hasEmptyPlace { get; set; }
        public Queue<UserAction> userActions { get; set; }
        public int blownLine { get; set; }
        public DateTime start { get; set; }
        public Timer aTimer { get; set; }
        public string playTime { get; set; }
        public bool addedResultDB { get; set; }
        public static int points { get; set; }
       

        public GameManager(int xBoard, int yBoard, int squareWidth, Context context)
        {
            this.SetManager(xBoard, yBoard, squareWidth, context);
            points = 0;
            this.start = DateTime.Now;
            this.SetTimer();
            this.playTime = "00:00:00";
            
        }

        public GameManager(int xBoard, int yBoard, int squareWidth, List<GraphicCard> cards, Context context)
        {
            this.SetManager(xBoard, yBoard, squareWidth, context);
            this.SetUpPreviousBoard(cards, context);
        }

        //set elements of the manager
        public void SetManager(int xBoard, int yBoard, int squareWidth, Context context)
        {
            graphicBoard = new GraphicBoard(xBoard, yBoard, squareWidth, Constants.ROWS_NUMBER, Constants.COLLUMS_NUMBER);
            ShapesOnBoard = new ShapesOnBoard(graphicBoard, context);
            this.blownLine = Constants.NONE;
            this.addedResultDB = false;
            this.userActions = new Queue<UserAction>();
        }

        //happen every certain amount of seconds
        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            if (hasEmptyPlace)
            {
                this.playTime = (e.SignalTime - this.start).ToString().Substring(0, Constants.SECONDS_INDEX_IN_TIMER);
                string s = (e.SignalTime - this.start).ToString();
                if (this.playTime[Constants.SECONDS_INDEX_IN_TIMER - 1] == '0')
                {
                    points += Constants.POINT_FOR_TEN_SEC;
                }
            }
        }

        private void SetTimer()
        {
            // Create a timer with a one second interval.
            this.aTimer = new Timer(1000);
            // Hook up the Elapsed event for the timer. 
            this.aTimer.Elapsed += OnTimedEvent;
            this.aTimer.AutoReset = true;
            this.aTimer.Enabled = true;
        }

        //get list of the cards from the last game and set the previos board'd time and points
        public void SetUpPreviousBoard(List<GraphicCard> cards, Context context)
        {
            this.SetTimer();

            string startEndPrevGame = cards[cards.Count - 1].Color;
            DateTime startPrevGame = DateTime.Parse(startEndPrevGame.Substring(0, startEndPrevGame.IndexOf('&')));
            DateTime endPrevGame = DateTime.Parse(startEndPrevGame.Substring(startEndPrevGame.IndexOf('&')+1));

            TimeSpan prevGameTime = endPrevGame - startPrevGame;
            this.start = DateTime.Now - prevGameTime;

            for(int i = 0; i < cards.Count - 1; i++)
            {
                this.ShapesOnBoard.AddPrevGameShape(new OneCard(cards[i], context));
                this.ShapesOnBoard.Shapes[this.ShapesOnBoard.Shapes.Count - 1].SetShape(this.graphicBoard);
            }
            GameManager.points = cards[cards.Count - 1].X;
        }


        public void CheckFullLine()
        {
            this.blownLine = this.graphicBoard.GetFullLine();
        }

        public void BlowLine()
        {
            this.graphicBoard.BlowLine(this.blownLine, this.ShapesOnBoard.Shapes);
        }

        public void ExtraPush()
        {
            this.ShapesOnBoard.ExtraPush(this.blownLine);
        }

        public void SetPlaceToShape()
        {
            this.graphicBoard.SetPlaceToShape(this.ShapesOnBoard.GetFaliingShape());
        }

        //get touch of user and handle it
        public void OnTouch(int eX, int eY, Arrow rightArrow, Arrow leftArrow)
        {
            if (this.graphicBoard.InSide(new GraphicPoint(eX, eY)))
            {
                //push rotate action into user actione queue
                UserAction rotate = new RotateAction(this.graphicBoard);
                this.userActions.Enqueue(rotate);
            }
            else
            {
                //push move action into user actione queue
                if (rightArrow.InSide(new GraphicPoint(eX, eY)))
                    this.MoveSides(rightArrow.Side);
                else if (leftArrow.InSide(new GraphicPoint(eX, eY)))
                    this.MoveSides(leftArrow.Side);
            }
        }

        //move the shape to sides
        public void MoveSides(string leftOrRIght)
        {
            if (leftOrRIght == Constants.LEFT)
            {
                UserAction moveLeft = new MoveAction(this.graphicBoard.GraphicCards, this.graphicBoard.cardWidth * -1);
                this.userActions.Enqueue(moveLeft);
            }
            else if (leftOrRIght == Constants.RIGHT)
            {
                UserAction moveRight = new MoveAction(this.graphicBoard.GraphicCards, this.graphicBoard.cardWidth);
                this.userActions.Enqueue(moveRight);
            }
        }

        public void AddResultToDB()
        {
            MainActivity.resultsDB.AddResult(new Result(points, DateTime.Now.ToString()));
            this.addedResultDB = true;
        }

        //add the board state to the db
        public void AddBoardToDB()
        {
            MainActivity.boardDB.DeleteAll();
            foreach(GraphicCard card in this.graphicBoard.GraphicCards)
            {
                DBcard dbCard = new DBcard(card.X, card.Y, card.width, card.finalPosInBoard.row, card.finalPosInBoard.col, card.Color);
                MainActivity.boardDB.AddCard(dbCard);
            }
        }

        //check if the shape is alreadt inside the board and set the member in Shape
        public void TryToSetShapeFullyOnBoard()
        {
            if (!this.ShapesOnBoard.GetFaliingShape().fullyOnBoard)
                this.ShapesOnBoard.GetFaliingShape().SetFullyOnBoard(this.graphicBoard);
        }
    }
}