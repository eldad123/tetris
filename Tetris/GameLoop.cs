﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris
{
    public class GameLoop
    {
        private const int TICKS_PER_SEC = 60;
        private const int MS_PER_TICK = 1000 / TICKS_PER_SEC;

        private bool isRunning = false;
        Task gameLoopTask;
        GameView gameView;

        private int ticksCountToFall;
        public GameLoop(GameView boardGame)
        {
            this.ticksCountToFall = 0;
            this.gameView = boardGame;
            isRunning = false;
        }

        /// <summary>
        /// Starts the game loop, running the game
        /// </summary>
        public void Start()
        {
            this.isRunning = true;
            gameLoopTask = Task.Run(() => Run());
        }


        private async Task Run()
        {
            DateTime nextLoop = DateTime.Now;
        
            while (isRunning)
            {
                while (nextLoop < DateTime.Now)
                {
                    gameView.Update(ref this.ticksCountToFall);
                    nextLoop = nextLoop.AddMilliseconds(MS_PER_TICK);
                    
                    //Improve performance
                    if (nextLoop > DateTime.Now)
                    {
                        await Task.Delay(nextLoop - DateTime.Now);
                        
                    }
            
                }
            }
        }
    }
}