﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tetris
{
    public abstract class UserAction
    {
        public bool isValid { get; set; }
        public UserAction()
        {
            this.isValid = true;
        }

        public abstract void DoAction(Shape shape); 
    }
}