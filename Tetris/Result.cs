﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tetris
{
    [Table("Results")]

    public class Result
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { set; get; }

        public int POINTS { get; set; }

        public string DATE { get; set; }

        public Result(int points, string date)
        {
            this.POINTS = points;
            this.DATE = date;
        }

        public Result()
        {
        }
    }
}