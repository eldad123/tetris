﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Tetris
{
    public class GraphicCard
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Dy { get; set; }
        public int width { get; set; }
        public Bitmap colorBmp { get; set; }
        public string Color { get; set; }
        public bool onBoard { get; set; }
        public GraphicPoint targetPnt { get; set; }

        public (int row, int col) finalPosInBoard { get; set; }
        public (int row, int col) posInBoard { get; set; }

        public GraphicCard(Bitmap bitmap, int width, string color)
        {
            this.finalPosInBoard = (-1, -1);
            this.posInBoard = (-1, -1);
            this.X = -1;
            this.Y = -1;
            this.Dy = 0;
            this.width = width;
            this.colorBmp = bitmap;
            this.Color = color;
            this.targetPnt = null;
        }

        public GraphicCard(int x, int y, Bitmap colorBmp)
        {
            this.finalPosInBoard = (-1, -1);
            this.posInBoard = (-1, -1);
            this.X = x;
            this.Y = y;
            this.Dy = 0;
            this.colorBmp = colorBmp;
            this.targetPnt = null;
        }

        public GraphicCard (GraphicCard other)
        {
            this.finalPosInBoard = other.finalPosInBoard;
            this.posInBoard = other.finalPosInBoard;
            this.X = other.X;
            this.Y = other.Y;
            this.Color = other.Color;
            this.width = other.width;
            this.Dy = other.Dy;
            this.colorBmp = other.colorBmp;
            this.targetPnt = other.targetPnt;
            this.onBoard = other.onBoard;
        }

        public GraphicCard(DBcard other, Context context)
        {
            this.finalPosInBoard = (other.ROW_IN_BOARD, other.COL_IN_BOARD);
            this.posInBoard = this.finalPosInBoard;
            this.X = other.X;
            this.Y = other.Y;
            this.width = other.WIDTH;
            this.Dy = this.width;
            this.Color = other.COLOR;
            this.targetPnt = new GraphicPoint(this.X, this.Y);
            this.onBoard = true;

            int imageId = context.Resources.GetIdentifier(this.Color, "drawable", context.PackageName);
            Bitmap bitmap = Android.Graphics.BitmapFactory.DecodeResource(Application.Context.Resources, imageId);
            this.colorBmp = bitmap;
        }

        public void SetPos(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public void Draw(Canvas canvas)
        {
            canvas.DrawBitmap(Bitmap.CreateScaledBitmap(this.colorBmp, (int)width, (int)width, true),
                (float)X, (float)Y, null);
        }

        public void Fall() //fall - and change the current position in the board
        {
            Y += Dy;
            if (this.onBoard)
                this.posInBoard = (this.posInBoard.row - 1, this.posInBoard.col);
        }

        public bool HasPosSet()
        {
            return this.X != -1 && this.Y != -1;
        }

        public void SetBitmap(Bitmap colorBmp)
        {
            this.colorBmp = colorBmp;
        }

        public void SetTargetPnt(GraphicPoint trgetPnt)
        {
            this.targetPnt = trgetPnt;
        }

        public void SetTargetDirection()
        {
            Dy = width;
        }

        //check if the card has reached to his target
        public bool OnTarget()
        {
            if (this.X == this.targetPnt.X && this.Y == this.targetPnt.Y)
            {
                Dy = 0;
                return true;
            }
            return false;
        }

        //check if given point is on the card
        public bool InSide(GraphicPoint p)
        {
            return (p.X >= this.X && p.X <= this.X + this.width) && (p.Y >= this.Y && p.Y <= this.Y + this.width);
        }

        //check if two cards are the same - by thier position in the board
        public override bool Equals(object obj)
        {
            return this.X == ((GraphicCard)obj).X && this.Y == ((GraphicCard)obj).Y;
        }
    }   
}