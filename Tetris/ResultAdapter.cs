﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tetris
{
    class ResultAdapter : BaseAdapter<Result>
    {

        List<Result> objects;
        Activity activity;                      // new

        //-------------------------------------------------------------
        public ResultAdapter(System.Collections.Generic.List<Result> objects,
                          Activity activity)  // new
        //-------------------------------------------------------------
        {
            this.objects = objects;
            this.activity = activity;
        }
        //-------------------------------------------------------------
        public List<Result> GetList()
        //-------------------------------------------------------------
        {
            return this.objects;
        }
        //-------------------------------------------------------------
        public override long GetItemId(int position)
        //-------------------------------------------------------------
        {
            return position;
        }
        //-------------------------------------------------------------
        public override int Count
        //-------------------------------------------------------------
        {
            get { return this.objects.Count; }
        }
        //-------------------------------------------------------------
        public override Result this[int position]
        //-------------------------------------------------------------
        {
            get { return this.objects[position]; }
        }
        //-------------------------------------------------------------
        public override View GetView(int position, View convertView, ViewGroup parent)
        //-------------------------------------------------------------
        {
            Android.Views.LayoutInflater layoutInflater = this.activity.LayoutInflater;// new
            Android.Views.View view = layoutInflater.Inflate(Resource.Layout.custom_layout, parent, false);

            TextView tvPlace = view.FindViewById<TextView>(Resource.Id.tvPlace);
            TextView tvResult = view.FindViewById<TextView>(Resource.Id.tvResult);
            TextView tvDate = view.FindViewById<TextView>(Resource.Id.tvDate);

            Result temp = objects[position];

            if (temp != null)
            {
                tvPlace.Text = (position+1).ToString();
                tvPlace.SetWidth(81);
                tvResult.Text = "Points: " + temp.POINTS.ToString();
                tvDate.Text = "Date: " + temp.DATE;

                switch(position + 1)
                {
                    case 1:
                        tvPlace.SetBackgroundColor(Android.Graphics.Color.Gold);
                        break;
                    case 2:
                        tvPlace.SetBackgroundColor(Android.Graphics.Color.Silver);
                        break;
                    case 3:
                        tvPlace.SetBackgroundColor(Android.Graphics.Color.ParseColor("#cd7f32"));
                        break;
                    default:
                        tvPlace.SetBackgroundColor(Android.Graphics.Color.OrangeRed);
                        break;

                }
            }
            return view;
        }
    }
}