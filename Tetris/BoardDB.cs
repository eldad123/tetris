﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using SQLite;

namespace Tetris
{
    public class BoardDB 
    {
        public string tableName { set; get; }
        public DBHandler DBHandler { get; set; }

        public BoardDB(DBHandler DBHandler, string tableName)
        {
            this.DBHandler = DBHandler;
            this.tableName = tableName;
        }

        public void DeleteAll()
        {
            string strsql = string.Format("DELETE FROM " + this.tableName);
            this.DBHandler.db.Query<DBcard>(strsql);
        }

        //get all the cards from the db
        public List<DBcard> GetAllCards()
        {
            List<DBcard> CardsList = new List<DBcard>();
            string strsql = string.Format("SELECT * FROM " + this.tableName);
            var Cards = this.DBHandler.db.Query<DBcard>(strsql);
            if (Cards.Count > 0)
            {
                foreach (var item in Cards)
                {
                    CardsList.Add(item);
                }
            }
            return CardsList;
        }

        
        public void AddCard(DBcard card)
        {
            this.DBHandler.db.Insert(card);
        }

        
    }
}