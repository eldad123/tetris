﻿using Android.App;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tetris
{
    [Service(Label = "MusicService")]//write service to menifest file 
    [IntentFilter(new String[] { "com.EldadBerger.MusicService" })]
    class BackgroundMusicService : Service
    {
        MediaPlayer mp;
        AudioManager am;

        [Obsolete]
        public override StartCommandResult OnStartCommand(Android.Content.Intent intent, StartCommandFlags flags, int startId)
        {
            // start your service logic here

            // משמיע את קובץ השמע
            mp = MediaPlayer.Create(this, Resource.Raw.tetrismusic);
            mp.Looping = true;
            mp.Start();

            // ==== משמש לשליטה ברכיב קול =======
            am = (AudioManager)GetSystemService(Context.AudioService);

            return StartCommandResult.Sticky;//במידה ונחדש פעילות של המוזיקה היא תמשיך מאותה נקודה
        }

        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            if (mp != null)
            {
                mp.Stop();
                mp.Release();
                mp = null;
            }

        }
    }
}