﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tetris
{
    public class ShapesOnBoard
    {
        public List<Shape> Shapes { get; set; }
        private GraphicBoard graphicBoard;
        private Context context;
        public ShapesOnBoard(GraphicBoard graphicBoard, Context context)
        {
            Shapes = new List<Shape>();
            this.graphicBoard = graphicBoard;
            this.context = context;
        }

        public bool AddSquare(int cardWidth)
        {
            Shape newShape = new SquareShape(cardWidth, this.context);
            return this.InitShape(newShape);
        }

        public bool AddLine(int cardWidth)
        {
            Shape newShape = new LineShape(cardWidth, this.context);
            return this.InitShape(newShape);
        }

        public bool AddPlus(int cardWidth)
        {
            Shape newShape = new PlusShape(cardWidth, this.context);
            return this.InitShape(newShape);
        }

        public bool AddL(int cardWidth)
        {
            Shape newShape = new LShape(cardWidth, this.context);
            return this.InitShape(newShape);
        }

        public bool AddJ(int cardWidth)
        {
            Shape newShape = new JShape(cardWidth, this.context);
            return this.InitShape(newShape);
        }

        public bool AddS(int cardWidth)
        {
            Shape newShape = new SShape(cardWidth, this.context);
            return this.InitShape(newShape);
        }

        public bool AddZ(int cardWidth)
        {
            Shape newShape = new ZShape(cardWidth, this.context);
            return this.InitShape(newShape);
        }
        
        //initialie new shape which has default place and target point
        private bool InitShape(Shape newShape)
        {
            newShape.SetShape(graphicBoard);
            bool succededToFindTrgt = newShape.GetFinalPlaceInBoard(graphicBoard);
            if (!succededToFindTrgt)
            {
                this.Shapes.Add(newShape);
                return false;
            }
            newShape.SetTargetDirection();
            this.Shapes.Add(newShape);
            return true;
        }

        public void Draw(Canvas canvas)
        {
            foreach (Shape shape in Shapes)
            {
                shape.Draw(canvas);
            }
        }
        
        //return if the falling shape has reached its target
        public bool FallingShapeOnTarget()
        {
            if (Shapes.Count > 0)
            {
                if (this.GetFaliingShape().OnTarget())
                    return true;
            }
            return false;
        }

        public void Fall()
        {
            if (Shapes.Count > 0)
            {
                lock(Shapes)
                {
                    this.GetFaliingShape().Fall();
                }
            }
        }

        //fall one step down the cards above the blown line
        public void ExtraPush(int row)
        {
            foreach (Shape shape in Shapes)
            {
                if(shape.AboveRow(row))
                {
                    shape.SetTargetDirection();
                    foreach (GraphicCard card in shape.activeCards)
                    {
                        if(card.posInBoard.row > row)
                        {
                            lock(card)
                            {
                                this.graphicBoard.GraphicCards[card.posInBoard.row, card.posInBoard.col] = null;
                                card.Fall();
                                card.finalPosInBoard = (card.finalPosInBoard.row - 1, card.finalPosInBoard.col);
                            }
                        }
                    }
                    graphicBoard.SetPlaceToShape(shape);
                }
            }
        }


        //add shape of previous game
        public void AddPrevGameShape(Shape newShape)
        {
            this.Shapes.Add(newShape);
        }

        //return the falling shape
        public Shape GetFaliingShape()
        {
            if(this.Shapes.Count > 0)
                return this.Shapes[this.Shapes.Count-1];
            return null;
        }


    }
}