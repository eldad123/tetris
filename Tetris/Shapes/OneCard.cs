﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tetris.Shapes
{
    class OneCard : Shape
    {
        public OneCard(GraphicCard card, Context context) : base(card, context)
        {

        }

        public override void SetShape(GraphicBoard graphicBoard)
        {
            graphicBoard.SetPlaceToShape(this);
        }
    }
}