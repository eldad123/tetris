﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tetris
{
    class SquareShape : Shape
    {
        public SquareShape(int cardWidth, Context context) : base(cardWidth, context)
        {
            

        }

        public override void SetShape(GraphicBoard graphicBoard)
        {
            int xBoard = graphicBoard.Left;
            int yBoard = graphicBoard.Top;
            int cardWidth = graphicBoard.cardWidth;
            int x1, y1;

            //second row
            x1 = xBoard + (cardWidth * (Constants.COLLUMS_NUMBER / 2 - 1));
            y1 = yBoard - cardWidth;
            Tetromino[2,1].SetPos(x1, y1);
            this.activeCards.Add(Tetromino[2, 1]);

            x1 = xBoard + (cardWidth * (Constants.COLLUMS_NUMBER / 2 ));
            y1 = yBoard - cardWidth;
            Tetromino[2,2].SetPos(x1, y1);
            this.activeCards.Add(Tetromino[2, 2]);


            //first row
            x1 = xBoard + (cardWidth * (Constants.COLLUMS_NUMBER / 2 -1));
            y1 = yBoard - 2*cardWidth;
            Tetromino[1,1].SetPos(x1, y1);
            this.activeCards.Add(Tetromino[1, 1]);

            x1 = xBoard + (cardWidth * (Constants.COLLUMS_NUMBER / 2 ));
            y1 = yBoard - 2*cardWidth;
            Tetromino[1,2].SetPos(x1, y1);
            this.activeCards.Add(Tetromino[1, 2]);

            this.PutNullInEmptyPlaces();
        }
    }
}