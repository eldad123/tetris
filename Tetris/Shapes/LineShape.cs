﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tetris
{
    class LineShape : Shape
    {
        public LineShape(int cardWidth, Context context) : base(cardWidth, context)
        {


        }

        public override void SetShape(GraphicBoard graphicBoard)
        {
            int xBoard = graphicBoard.Left;
            int yBoard = graphicBoard.Top;
            int cardWidth = graphicBoard.cardWidth;
            int x1, y1;

            x1 = xBoard + (cardWidth * (Constants.COLLUMS_NUMBER / 2 - 2));
            y1 = yBoard - cardWidth;
            this.Tetromino[0, 0].SetPos(x1, y1);
            this.activeCards.Add(this.Tetromino[0, 0]);

            x1 = xBoard + (cardWidth * (Constants.COLLUMS_NUMBER / 2 - 1));
            y1 = yBoard - cardWidth;
            this.Tetromino[0, 1].SetPos(x1, y1);
            this.activeCards.Add(this.Tetromino[0, 1]);

            x1 = xBoard + (cardWidth * (Constants.COLLUMS_NUMBER / 2 ));
            y1 = yBoard - cardWidth;
            this.Tetromino[0, 2].SetPos(x1, y1);
            this.activeCards.Add(this.Tetromino[0, 2]);

            x1 = xBoard + (cardWidth * (Constants.COLLUMS_NUMBER / 2 + 1));
            y1 = yBoard - cardWidth;
            this.Tetromino[0,3].SetPos(x1, y1);
            this.activeCards.Add(this.Tetromino[0, 3]);


            this.PutNullInEmptyPlaces();
        }
    }
}