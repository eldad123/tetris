﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tetris
{
    public class GraphicPoint
    {
        public int X { get; set; }
        public int Y { get; set; }

        public GraphicPoint()
        {
        }
        public GraphicPoint(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}