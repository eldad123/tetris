﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tetris
{
    class RotateAction : UserAction
    {
        private GraphicBoard graphicBoard;
        public RotateAction(GraphicBoard graphicBoard) : base()
        {
            this.graphicBoard = graphicBoard;
        }

        public override void DoAction(Shape shape) //do rotating
        {
            GraphicCard[,] tetromino = shape.Tetromino;
            GraphicCard[,] rotatedTetromino = new GraphicCard[Constants.TETROMINO_LEN, Constants.TETROMINO_LEN];
            //copy the tetromino
            for (int i = 0; i < tetromino.GetLength(0); i++)
            {
                for (int j = 0; j < tetromino.GetLength(1); j++)
                {
                    if(tetromino[i, j] != null)
                        rotatedTetromino[i, j] = new GraphicCard(tetromino[i,j]);
                }
            }
            this.RotateTetromino(rotatedTetromino); //rotate the copied tetromino

            //Creare dictionary of the card in the teromino to their position in the tetromino
            Dictionary<GraphicCard, (int row, int col)> cardTotPosOrigin = new Dictionary<GraphicCard, (int row, int col)>();
            Dictionary<GraphicCard, (int row, int col)> cardTotPosRotated = new Dictionary<GraphicCard, (int row, int col)>();
            FillDict(cardTotPosOrigin, tetromino);
            FillDict(cardTotPosRotated, rotatedTetromino);

            foreach (KeyValuePair<GraphicCard, (int row, int col)> originCard in cardTotPosOrigin)
            {
                //check how the cardmoved due to the rotating and do do in pixels
                (int row, int col) newTetPos = cardTotPosRotated.FirstOrDefault(c => c.Key.Equals(originCard.Key)).Value;

                int rowDiff = newTetPos.row - originCard.Value.row;
                int colDiff = newTetPos.col - originCard.Value.col;

                int newX = tetromino[originCard.Value.row, originCard.Value.col].X + colDiff * this.graphicBoard.cardWidth;
                int newY = tetromino[originCard.Value.row, originCard.Value.col].Y + rowDiff * this.graphicBoard.cardWidth;

                (int row, int col) newBoardPos = this.graphicBoard.PointToPosInArray(new GraphicPoint(newX, newY));

                if (newBoardPos == (-1, -1) || this.graphicBoard.GraphicCards[newBoardPos.row, newBoardPos.col] != null)
                {
                    this.isValid = false;
                    break;
                }
                else
                {
                    rotatedTetromino[newTetPos.row, newTetPos.col].X += colDiff * this.graphicBoard.cardWidth;
                    rotatedTetromino[newTetPos.row, newTetPos.col].Y += rowDiff * this.graphicBoard.cardWidth;
                    rotatedTetromino[newTetPos.row, newTetPos.col].posInBoard = newBoardPos;
                    this.isValid = true;
                }

            }

            if (this.isValid)
            {
                shape.Tetromino = rotatedTetromino;
                lock (shape.activeCards)
                {
                    shape.activeCards.Clear();
                    foreach (GraphicCard card in shape.Tetromino)
                    {
                        if (card != null)
                            shape.activeCards.Add(card);
                    }
                }

            }

        }

        private void RotateTetromino(GraphicCard[,] tetromino)
        {
            int N = tetromino.GetLength(0);
            for (int i = 0; i < N / 2; i++)
            {
                for (int j = i; j < N - i - 1; j++)
                {
                    GraphicCard temp = tetromino[i, j];
                    tetromino[i, j] = tetromino[N - 1 - j, i];
                    tetromino[N - 1 - j, i] = tetromino[N - 1 - i, N - 1 - j];
                    tetromino[N - 1 - i, N - 1 - j] = tetromino[j, N - 1 - i];
                    tetromino[j, N - 1 - i] = temp;
                }
            }
        }

        private void FillDict(Dictionary<GraphicCard, (int row, int col)> dict, GraphicCard[,] tet)
        {
            for (int i = 0; i < Constants.TETROMINO_LEN; i++)
            {
                for (int j = 0; j < Constants.TETROMINO_LEN; j++)
                {
                    if (tet[i, j] != null)
                        dict.Add(new GraphicCard(tet[i, j]), (i, j));
                }
            }
        }
    }
}