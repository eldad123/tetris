﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Content;
using System.Collections.Generic;
using Android.Views;


namespace Tetris
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : MenuActivity
    {
        public static DBHandler db = new DBHandler("TETRIS_DB");
        public static ResultsDB resultsDB = new ResultsDB(db, "Results");
        public static BoardDB boardDB = new BoardDB(db, "Cards");
        
        Button btnStartGame;
        Button btnStats;
        Button btnRules;

        TextView battery;
        BroadcastBattery broadCastBattery;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
            btnStartGame = FindViewById<Button>(Resource.Id.btnStartGame);
            btnStats = FindViewById<Button>(Resource.Id.btnStats);
            btnRules = FindViewById<Button>(Resource.Id.btnRules);
            
            battery = FindViewById<TextView>(Resource.Id.battery);
            broadCastBattery = new BroadcastBattery(battery);

            btnStartGame.Click += BtnStartGame_Click;
            btnStats.Click += BtnStats_Click;
            btnRules.Click += BtnRules_Click;
            ResultsActivity.resultsList = new List<Result>();
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
       
        private void BtnStartGame_Click(object sender, System.EventArgs e)
        {
            Intent intent = new Intent(this, typeof(GameActivity));
            StartActivity(intent);
        }

        private void BtnStats_Click(object sender, System.EventArgs e)
        {
            Intent intent = new Intent(this, typeof(ResultsActivity));
            StartActivity(intent);
        }

        private void BtnRules_Click(object sender, System.EventArgs e)
        {
            Intent intent = new Intent(this, typeof(RulesActivity));
            StartActivity(intent);
        }

        protected override void OnResume()
        {
            base.OnResume();
            //set the battery tracker
            RegisterReceiver(broadCastBattery, new IntentFilter(Intent.ActionBatteryChanged));
        }

        protected override void OnPause()
        {
            base.OnPause();
            UnregisterReceiver(broadCastBattery);
        }

    }
}