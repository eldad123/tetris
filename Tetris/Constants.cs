﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tetris
{
    static class Constants
    {
        public static readonly string[] BITMAPS_NAMES = { "azure", "blue", "green", "orange", "purple", "red", "yellow" };
        public static readonly int ROWS_NUMBER = 19;
        public static readonly int COLLUMS_NUMBER = 10;
        public static readonly int SHAPES_NUMBER = 7;
        public static readonly string LEFT = "left";
        public static readonly string RIGHT = "right";
        public static readonly int TETROMINO_LEN = 4;
        public static readonly int NONE = -1;
        public static readonly int SECONDS_INDEX_IN_TIMER = 8;
        public static readonly int POINT_FOR_BLOW_LINE = 10;
        public static readonly int POINT_FOR_TEN_SEC = 5;
    }
}