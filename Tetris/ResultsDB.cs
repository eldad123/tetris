﻿using System;
using System.Collections.Generic;
using System.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Linq;
using System.Text;
using SQLite;

namespace Tetris
{
    public class ResultsDB
    {
        public string tableName { set; get; }
        public DBHandler DBHandler { get; set; }

        public ResultsDB(DBHandler DBHandler, string tableName) 
        {
            this.DBHandler = DBHandler;
            this.tableName = tableName;
        }

        public void DeleteAll()
        {
            string strsql = string.Format("DELETE FROM " + this.tableName );
            this.DBHandler.db.Query<Result>(strsql);
        }

        //get all the results from the best to worst
        public List<Result> GetAllResults()
        {
            List<Result> ResultsList = new List<Result>();
            string strsql = string.Format("SELECT * FROM " + this.tableName + " ORDER BY POINTS DESC");
            var Results = this.DBHandler.db.Query<Result>(strsql);
            if (Results.Count > 0)
            {
                foreach (var item in Results)
                {
                    ResultsList.Add(item);
                }
            }
            return ResultsList;
        }

        public List<Result> GetResultsOrderByDesc(int points)
        {
            List<Result> ResultsList = new List<Result>();
            string strsql = string.Format("SELECT * FROM " + this.tableName + " ORDER BY POINTS DESC");
            var Results = this.DBHandler.db.Query<Result>(strsql);
            if (Results.Count > 0)
            {
                foreach (var item in Results)
                {
                    ResultsList.Add(item);
                }
            }
            return ResultsList;
        }

        public void AddResult(Result result)
        {
            this.DBHandler.db.Insert(result);
        }

        public void DeleteResult(int id)
        {
            this.DBHandler.db.Delete<Result>(id);
        }

        public override string ToString()
        {
            string allResults = "";
            foreach (Result Result in this.GetAllResults())
            {
                allResults += Result.ToString() + "\n";
            }
            return allResults;
        }

    }
}