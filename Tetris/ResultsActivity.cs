﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tetris
{
    [Activity(Label = "Results")]
    public class ResultsActivity : MenuActivity, AdapterView.IOnItemClickListener, ListView.IOnItemLongClickListener
    {
        ListView lv;
        public static List<Result> resultsList { get; set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.results_layout);
            lv = FindViewById<ListView>(Resource.Id.lv);
         //   MainActivity.db.DeleteAll();
            resultsList = MainActivity.resultsDB.GetAllResults();
            lv.Adapter = new ResultAdapter(resultsList, this);

            lv.OnItemClickListener = (AdapterView.IOnItemClickListener)this;
            lv.OnItemLongClickListener = (AdapterView.IOnItemLongClickListener)this;
        }

        public bool OnItemLongClick(AdapterView parent, View view, int position, long id)
        {
            MainActivity.resultsDB.DeleteResult(resultsList[position].Id);
            resultsList.RemoveAt(position);
            lv.Adapter = new ResultAdapter(resultsList, this);

            return true;
        }

        public void OnItemClick(AdapterView parent, View view, int position, long id)
        {
   
        }

        protected override void OnResume()
        {
            base.OnResume();
            lv.Adapter = new ResultAdapter(resultsList, this);
        }


    }
}