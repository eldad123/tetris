﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Essentials;

namespace Tetris
{
    [Activity(Label = "GameActivity", Theme = "@style/Theme.AppCompat.Light.NoActionBar")]
    public class GameActivity : MenuActivity
    {
        GameView gameView;
        RelativeLayout relative;
        Dialog dialogLoadPrevGame;
        Button BtnLoad;
        Button BtnDontLoad;
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.game_layout);
            
            relative = FindViewById<RelativeLayout>(Resource.Id.rltv);

            if(MainActivity.boardDB.GetAllCards().Count != 0) //if there is board in db
                CreateLoadPrevGameDialog();
            else
            {
                gameView = new GameView(this);
                relative.AddView(gameView);
            }

            //MainActivity.boardDB.DeleteAll(); 
        }

        public void CreateLoadPrevGameDialog()
        {
            dialogLoadPrevGame = new Dialog(this);
            dialogLoadPrevGame.SetContentView(Resource.Layout.load_prev_game_layout);
            dialogLoadPrevGame.SetCancelable(false);
            
            BtnLoad = dialogLoadPrevGame.FindViewById<Button>(Resource.Id.BtnLoad);
            BtnDontLoad = dialogLoadPrevGame.FindViewById<Button>(Resource.Id.BtnDontLoad);

            BtnLoad.Click += BtnLoadPrevGame_Click;
            BtnDontLoad.Click += BtnDontLoadPrevGame_Click;

            dialogLoadPrevGame.Show();
        }

        //save the current board in the db
        public void SaveGame()
        {
            MainActivity.boardDB.DeleteAll();
            for (int i = 0; i < this.gameView.gameManager.ShapesOnBoard.Shapes.Count - 1; i++)
            {
                foreach (GraphicCard card in this.gameView.gameManager.ShapesOnBoard.Shapes[i].activeCards)
                {
                    MainActivity.boardDB.AddCard(new DBcard(card));
                }
            }
            MainActivity.boardDB.AddCard(new DBcard(GameManager.points, -1, -1, -1, -1,
                this.gameView.gameManager.start.ToString() + "&" + DateTime.Now.ToString()));
        }

        //load the previous board from db
        public List<GraphicCard> LoadGame()
        {
            List<DBcard> dbCards = MainActivity.boardDB.GetAllCards();
            List<GraphicCard> cards = new List<GraphicCard>();
            foreach(DBcard dbCard in dbCards)
            {
                cards.Add(new GraphicCard(dbCard, this));
            }

            return cards;
        }

        //add the view with the last game
        private void BtnLoadPrevGame_Click(object sender, System.EventArgs e)
        {
            gameView = new GameView(this.LoadGame(), this);
            relative.AddView(gameView);
            dialogLoadPrevGame.Dismiss();
        }

        //add the view
        private void BtnDontLoadPrevGame_Click(object sender, System.EventArgs e)
        {
            gameView = new GameView(this);
            relative.AddView(gameView);
            dialogLoadPrevGame.Dismiss();
        }


        protected override void OnResume()
        {
            base.OnResume();
            Intent intent = new Intent(this, typeof(BackgroundMusicService));
            StartService(intent);
        }

        protected override void OnPause()
        {
            base.OnPause();
            if (!gameView.gameManager.addedResultDB)
                SaveGame();
            Intent intent = new Intent(this, typeof(BackgroundMusicService));
            StopService(intent);
            Finish();
        }


        public void GoBackToMain()
        {
            Intent intent = new Intent(this, typeof(MainActivity));
            StartActivity(intent);
        }

    }
}