﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System.Text;
using Xamarin.Essentials;

namespace Tetris
{
    public class GameView : View
    {
        public GameManager gameManager { get; set; }
        private int screenWidth { get; set; }
        private int screenHeight { get; set; }

        public Dialog popUpGameOver;   
        
        private GameLoop gameLoop;

        private const int TICKS_TO_FALL = 20;
        public Arrow leftArrow { set; get; }
        public Arrow rightArrow { set; get; }
        public Paint textPaint { get; set; }

        Button BtnBack { get; set; }
        public Context ViewContext { get; set; }

        public GameView(Context context)
            : base(context)
        {
            this.ViewContext = context;
            CreateGameElements(context);

            int xBoard = (int)(this.screenWidth * 0.14);
            int yBoard = (int)(this.screenHeight * 0.15);
            int squareWidth = (int)( ((int) this.screenHeight - yBoard) * 0.04);

            this.gameManager = new GameManager(xBoard, yBoard, squareWidth, context);
            
            CreateArrows((int)screenHeight, (int)screenWidth, context);

            this.gameManager.hasEmptyPlace = this.RandNewShape();

            gameLoop = new GameLoop(this);
            gameLoop.Start();
        }

        public GameView(List<GraphicCard> cards, Context context) : base(context)
        {
            CreateGameElements(context);

            int xBoard = (int)(this.screenWidth * 0.14);
            int yBoard = (int)(this.screenHeight * 0.15);
            int squareWidth = (int)(((int)this.screenHeight - yBoard) * 0.04);

            this.gameManager = new GameManager(xBoard, yBoard, squareWidth, cards, context);

            CreateArrows((int)screenHeight, (int)screenWidth, context);

            this.gameManager.hasEmptyPlace = this.RandNewShape();

            gameLoop = new GameLoop(this);
            gameLoop.Start();
        }

        //create paints and dialogs and set sizes
        public void CreateGameElements(Context context)
        {
            var mainDisplayInfo = DeviceDisplay.MainDisplayInfo;
            this.screenWidth = (int)mainDisplayInfo.Width;
            this.screenHeight = (int)mainDisplayInfo.Height;

            this.popUpGameOver = new Dialog(context);
            this.popUpGameOver.SetContentView(Resource.Layout.game_over_layout);
            this.popUpGameOver.SetCancelable(false);
            this.BtnBack = popUpGameOver.FindViewById<Button>(Resource.Id.BtnBack);
            this.BtnBack.Click += this.BtnBackToMenu_Click;
            textPaint = new Paint();
            textPaint.Color = Android.Graphics.Color.DarkOliveGreen;
            textPaint.StrokeWidth = 100;
            textPaint.TextSize = 70;
        }

        //create the arrows of the game
        public void CreateArrows(int screenHeight, int screenWidth, Context context)
        {
            int yArrows = screenHeight - (int)(this.gameManager.graphicBoard.Buttom * 0.25);
            int arrowsHeight = (int)(((int)screenHeight - (this.gameManager.graphicBoard.Buttom)) * 0.4);
            int arrowsWidth = (int)(screenWidth * 0.3);
            int leftArrowX = (int)(screenWidth * 0.12);
            int rightArrowX = (int)(screenWidth * 0.88) - arrowsWidth;

            int imageId = context.Resources.GetIdentifier("left_arrow", "drawable", context.PackageName);
            Bitmap bitmap = Android.Graphics.BitmapFactory.DecodeResource(Application.Context.Resources, imageId);
            leftArrow = new Arrow(bitmap, leftArrowX, yArrows, arrowsHeight, arrowsWidth, Constants.LEFT);

            imageId = context.Resources.GetIdentifier("right_arrow", "drawable", context.PackageName);
            bitmap = Android.Graphics.BitmapFactory.DecodeResource(Application.Context.Resources, imageId);
            rightArrow = new Arrow(bitmap, rightArrowX, yArrows, arrowsHeight, arrowsWidth, Constants.RIGHT);
        }


        //upsate boardview - user actions, falling, and drawing
        public void Update(ref int ticksCountToFall)
        {
            lock (this.gameManager.userActions)
            {
                while (this.gameManager.userActions.Count() != 0 && gameManager.ShapesOnBoard.GetFaliingShape().fullyOnBoard)
                {
                    this.gameManager.userActions.Peek().DoAction(this.gameManager.ShapesOnBoard.GetFaliingShape());
                    if (this.gameManager.userActions.Dequeue().isValid)
                        this.gameManager.ShapesOnBoard.GetFaliingShape().GetFinalPlaceInBoard(this.gameManager.graphicBoard);
                }
            }

            if (gameManager.hasEmptyPlace)
                Invalidate();
           
          
            if (ticksCountToFall == TICKS_TO_FALL && this.gameManager.hasEmptyPlace)
            {
                this.gameManager.ShapesOnBoard.Fall();
                ticksCountToFall = 0;
            }
            else
            {
                ticksCountToFall++;
            }
            
        }

        //draw the elements and check things that hapenning in board
        protected override void OnDraw(Canvas canvas)
        {           
            base.OnDraw(canvas);
            canvas.DrawText("Points: " + GameManager.points, (int)(this.screenWidth * 0.17), (int)(this.screenHeight * 0.07), this.textPaint);
            canvas.DrawText("Time: " + gameManager.playTime, (int)(this.screenWidth * 0.17), (int)(this.screenHeight * 0.12), this.textPaint);

            leftArrow.Draw(canvas);
            rightArrow.Draw(canvas);

            gameManager.graphicBoard.Draw(canvas);
            gameManager.ShapesOnBoard.Draw(canvas);

            if (this.gameManager.ShapesOnBoard.FallingShapeOnTarget() && this.gameManager.hasEmptyPlace)
            {
                this.gameManager.TryToSetShapeFullyOnBoard();

                this.gameManager.SetPlaceToShape();
                this.gameManager.CheckFullLine();
                //check if need to blow line
                while (gameManager.blownLine != Constants.NONE)
                {
                    this.gameManager.BlowLine();
                    this.gameManager.ExtraPush();
                    GameManager.points += Constants.POINT_FOR_BLOW_LINE;
                    this.gameManager.CheckFullLine();
                }
                this.gameManager.hasEmptyPlace = this.RandNewShape(); //try to create another falling shape
            }
            else
            {
                this.gameManager.TryToSetShapeFullyOnBoard();
            }

            //game over
            if (!this.gameManager.hasEmptyPlace && !this.gameManager.addedResultDB)
            {
                this.gameManager.AddResultToDB();
                MainActivity.boardDB.DeleteAll();
                this.popUpGameOver.Show();
            }
        }


        public override bool OnTouchEvent(MotionEvent e)
        {
            int eX = (int)e.GetX();
            int eY = (int)e.GetY();
            if (e.Action == MotionEventActions.Up)
            {
                this.gameManager.OnTouch(eX, eY, rightArrow, leftArrow);
            }
            return true;
        }

        //function to create random falling shape
        private bool RandNewShape()
        {
            Random rndShape = new Random();
            bool succededToAdd = false;
            switch (rndShape.Next(0, Constants.SHAPES_NUMBER))
            {
                case 0:
                    succededToAdd = gameManager.ShapesOnBoard.AddLine(gameManager.graphicBoard.cardWidth);
                    break;
                case 1:
                    succededToAdd = gameManager.ShapesOnBoard.AddSquare(gameManager.graphicBoard.cardWidth);
                    break;
                case 2:
                    succededToAdd = gameManager.ShapesOnBoard.AddPlus(gameManager.graphicBoard.cardWidth);
                    break;
                case 3:
                    succededToAdd = gameManager.ShapesOnBoard.AddL(gameManager.graphicBoard.cardWidth);
                    break;
                case 4:
                    succededToAdd = gameManager.ShapesOnBoard.AddJ(gameManager.graphicBoard.cardWidth);
                    break;
                case 5:
                    succededToAdd = gameManager.ShapesOnBoard.AddS(gameManager.graphicBoard.cardWidth);
                    break;
                case 6:
                    succededToAdd = gameManager.ShapesOnBoard.AddZ(gameManager.graphicBoard.cardWidth);
                    break;
            }
            return succededToAdd;
        }

        private void BtnBackToMenu_Click(object sender, System.EventArgs e)
        {
            ((GameActivity)this.Context).GoBackToMain();
        }


    }

    
}