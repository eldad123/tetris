﻿using Android.Graphics;
using System.Collections.Generic;
using System.Linq;

namespace Tetris
{
    public class GraphicBoard
    {
        public Paint linesPaint { get; set; }
        public Paint backgroundPaint { get; set; }
        public int Left { get; set; }
        public int Top { get; set; }
        public int Buttom { get; set; }
        public int cardWidth { get; set; }
        public int Rows { get; set; }
        public int Cols { get; set; }

        public GraphicCard[,] GraphicCards { get; set; }

        public GraphicBoard(int x, int y, int cardWidth, int rows, int cols)
        {
            this.Left = x;
            this.Top = y;
            this.cardWidth = (int)cardWidth;
            this.Rows = rows;
            this.Cols = cols;
            this.Buttom = this.Top + this.Rows * this.cardWidth;

            GraphicCards = new GraphicCard[rows, cols];
            backgroundPaint = new Paint();
            backgroundPaint.Color = Android.Graphics.Color.MediumPurple;
            backgroundPaint.StrokeWidth = 4;

            linesPaint = new Paint();
            linesPaint.Color = Android.Graphics.Color.Black;
            linesPaint.StrokeWidth = 4;
        }

        //Draw the board of the game
        public void Draw(Canvas canvas)
        {
            int x1 = Left;
            int y1 = Top;

            canvas.DrawRect((float)Left, (float)Top, (float)Left + (Cols * cardWidth), (float)Top + (Rows * cardWidth), this.backgroundPaint);

            for (int i = 0; i < Rows + 1; i++)
            {
                canvas.DrawLine((float)x1, (float)y1, (float)x1 + (Cols * cardWidth), (float)y1, this.linesPaint);
                x1 = Left;
                y1 += cardWidth;
            }
            x1 = Left;
            y1 = Top;
            for (int i = 0; i < Cols + 1; i++)
            {
                canvas.DrawLine((float)x1, (float)y1, (float)x1, (float)y1 + (Rows * cardWidth), this.linesPaint);
                y1 = Top;
                x1 += cardWidth;
            }

        }

        //convert point in the screen to position in the board
        public (int, int) PointToPosInArray(GraphicPoint p)
        {
            int x1 = this.Left;
            int y1 = this.Top + this.cardWidth * (this.Rows - 1);
            for (int i = 0; i < this.Rows; i++)
            {
                for (int j = 0; j < this.Cols; j++)
                {
                    if (p.X == x1 && p.Y == y1)
                        return (i, j);
                    x1 += cardWidth;
                }
                x1 = this.Left;
                y1 -= this.cardWidth;
            }
            return (Constants.NONE, Constants.NONE);
        }

        //get the collum in the board by its x value
        public int GetColFromX(int x)
        {
            int x1 = this.Left;
            int y1 = this.Top + this.cardWidth * (this.Rows - 1);
            for (int j = 0; j < this.Cols; j++)
            {
                if (x1 == x)
                    return (j);
                x1 += cardWidth;
            }

            return Constants.NONE;
        }

        public int GetRowFromY(int y)
        {
            int x1 = this.Left;
            int y1 = this.Top + this.cardWidth * (this.Rows - 1);
            for (int i = 0; i < this.Rows; i++)
            {
                if (y1 == y)
                    return (i);
                y1 -= cardWidth;
            }

            return Constants.NONE;
        }

        /*public int GetFirstEmptyRowInCol(int col, int maxRow)
        {
            maxRow = maxRow == -1 ? Constants.rowsNumber - 1 : maxRow;
            for (int row = maxRow; row >=0; row--)
            {
                if (this.GraphicCards[row, col] == null)
                {
                    return row;
                }      
            }

            return -1;
        }*/

        //function get collum in board and return the highest row in this col that empty
        public int GetFirstEmptyRowInCol(int col, int maxRow)
        {
            maxRow = maxRow == -1 ? Constants.ROWS_NUMBER - 1 : maxRow;
            int savedRow = this.GraphicCards.GetLength(0) - 1;

            for (int row = maxRow; row >= 0; row--)
            {
                if (this.GraphicCards[row, col] == null)
                {
                    if (row == 0)
                        return 0;
                    savedRow = row;
                }
                if (this.GraphicCards[row, col] != null)
                {
                    if (row == this.GraphicCards.GetLength(0) - 1)
                        return -1;
                    return savedRow;
                }
            }

            return Constants.NONE;
        }


        //convert position in board to point in the screen 
        public GraphicPoint GetPointPromPosInArray(int row, int col)
        {
            int x1 = this.Left;
            int y1 = this.Top + this.cardWidth * (this.Rows - 1);
            for (int i = 0; i < this.Rows; i++)
            {
                for (int j = 0; j < this.Cols; j++)
                {
                    if (row == i && j == col)
                        return new GraphicPoint(x1, y1);
                    x1 += cardWidth;
                }
                x1 = this.Left;
                y1 -= this.cardWidth;
            }
            return null;
        }

        //set place to the shape in the board - take the place
        public void SetPlaceToShape(Shape shape)
        {
            foreach (GraphicCard card in shape.activeCards)
            {
                if (card != null)
                {
                    //card.posInBoard = card.finalPosInBoard;
                    (int, int) a = card.finalPosInBoard;
                    card.finalPosInBoard = card.posInBoard;
                    this.GraphicCards[card.posInBoard.row, card.posInBoard.col] = card;
                }
            }
        }


        //get full line in the board - if there is no one return -1
        public int GetFullLine()
        {
            for (int i = 0; i < this.Rows; i++)
            {
                bool full = true;
                for (int j = 0; j < this.Cols; j++)
                {
                    if (this.GraphicCards[i, j] == null)
                    {
                        full = false;
                        break;
                    }
                }
                if (full)
                    return i;
            }
            return Constants.NONE;
        }


        //set cards you need to blow in shapes to null
        public void BlowLine(int blownRow, List<Shape> shapes)
        {
            for(int col = 0; col < this.Cols; col++)
            {
                this.GraphicCards[blownRow, col] = null;
            }
            lock (shapes)
            {
                foreach (Shape shape in shapes)
                {
                    shape.activeCards.RemoveAll(c => c.posInBoard.row == blownRow); 
                    /*if (shape.activeCards.Count() == 0)
                    {
                        shapes.Remove(shape);
                        continue;
                    }*/
                    
                    for (int i = 0; i < shape.Tetromino.GetLength(0); i++)
                    {
                        for (int j = 0; j < shape.Tetromino.GetLength(1); j++)
                        {
                            if (shape.Tetromino[i, j] != null && shape.Tetromino[i, j].posInBoard.row == blownRow)
                                shape.Tetromino[i, j] = null;
                        }
                    }
                }

                //shapes.RemoveAll(s => s.activeCards.Count() == 0);               
            }
        }

        public bool InSide(GraphicPoint p)
        {
            return (p.X >= this.Left && p.X <= this.Left + this.cardWidth*this.Cols) && 
                (p.Y >= this.Top && p.Y <= this.Top + this.Rows*this.cardWidth);
        }

    }
}